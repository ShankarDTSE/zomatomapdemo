package com.huawei.ZomatoMapDemo;

import android.os.Bundle;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;

import com.huawei.hms.maps.HuaweiMap;
import com.huawei.hms.maps.MapFragment;
import com.huawei.hms.maps.OnMapReadyCallback;


public abstract class BaseDemoActivity extends FragmentActivity implements OnMapReadyCallback, HuaweiMap.OnMarkerClickListener {
    private HuaweiMap mMap;
    private boolean mIsRestore;
    private MapFragment mMapFragment;

    protected int getLayoutId() {
        return R.layout.mapview;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mIsRestore = savedInstanceState != null;
        setContentView(getLayoutId());
        setUpMap();

    }

    @Override
    public void onMapReady(HuaweiMap map) {
        if (mMap != null) {
            return;
        }
        mMap = map;
        mMap.setMyLocationEnabled(true);

        startDemo(mIsRestore);
    }

    private void setUpMap() {
        mMapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.mapView);
        mMapFragment.getMapAsync(this);
    }

    /**
     * Run the demo-specific code.
     */
    protected abstract void startDemo(boolean isRestore);

    protected HuaweiMap getMap() {
        return mMap;
    }
}
