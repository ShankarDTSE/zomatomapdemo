package com.huawei.ZomatoMapDemo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.icon_gen_id).setOnClickListener(this);
        findViewById(R.id.poly_simply_id).setOnClickListener(this);
        findViewById(R.id.poly_decode_id).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.poly_simply_id:
                Intent polyDecode = new Intent(MainActivity.this, PolySimplifyDemoActivity.class);
                startActivity(polyDecode);
                break;
            case R.id.poly_decode_id:
                Intent polySim = new Intent(MainActivity.this, PolyDecodeDemoActivity.class);
                startActivity(polySim);
                break;
            case R.id.icon_gen_id:
                // awarenessKit();
                Intent callIntent = new Intent(MainActivity.this, IconGeneratorDemoActivity.class);
                startActivity(callIntent);
                break;


            default:
                break;
        }
    }
}
